(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default').primaryPalette('pink').accentPalette('cyan');
  });
})();