(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.config(function($routeProvider, SECURED_ROUTES) {
    // credits for this idea: https://groups.google.com/forum/#!msg/angular/dPr9BpIZID0/MgWVluo_Tg8J
    // unfortunately, a decorator cannot be use here because they are not applied until after
    // the .config calls resolve, so they can't be used during route configuration, so we have
    // to hack it directly onto the $routeProvider object
    $routeProvider.whenAuthenticated = function(path, route) {
      route.resolve = route.resolve || {};
      route.resolve.user = ['Auth', function(Auth) {
        return Auth.$requireAuth();
      }];
      $routeProvider.when(path, route);
      SECURED_ROUTES[path] = true;
      return $routeProvider;
    };
  })
	.config(function ($routeProvider) {
    $routeProvider
			.whenAuthenticated('/quotes/:quoteId', {
				templateUrl: 'views/quote.html',
				controller: 'QuotesCtrl'
			})
      .whenAuthenticated('/quotes', {
        templateUrl: 'views/quotes.html',
        controller: 'QuotesCtrl'
      })
      .whenAuthenticated('/pricing/makes/:makeId/models/:modelId', {
				templateUrl: 'views/pricing/model.html',
				controller: 'PricingCtrl'
      })
      .whenAuthenticated('/pricing/makes/:makeId/models', {
				templateUrl: 'views/pricing/models.html',
				controller: 'PricingCtrl'
      })
      .whenAuthenticated('/pricing/makes', {
				templateUrl: 'views/pricing/makes.html',
				controller: 'MakesCtrl'
      })
      .whenAuthenticated('/makes/:makeId/models/:modelId/styles/add', {
        templateUrl: 'views/styles/style-add.html',
        controller: 'StylesCtrl'
      })
      .whenAuthenticated('/makes/:makeId/models/:modelId/styles/:styleId', {
        templateUrl: 'views/styles/style.html',
        controller: 'StylesCtrl'
      })
      .whenAuthenticated('/makes/:makeId/models/:modelId/styles', {
        templateUrl: 'views/styles/styles.html',
        controller: 'StylesCtrl'
      })
      .whenAuthenticated('/makes/:makeId/models/add', {
        templateUrl: 'views/models/model-add.html',
        controller: 'ModelsCtrl'
      })
      .whenAuthenticated('/makes/:makeId/models/:modelId', {
        templateUrl: 'views/models/model.html',
        controller: 'ModelsCtrl'
      })
      .whenAuthenticated('/makes/:makeId/models', {
        templateUrl: 'views/models/models.html',
        controller: 'ModelsCtrl'
      })
      .whenAuthenticated('/makes/add', {
        templateUrl: 'views/makes/make-add.html',
        controller: 'MakesCtrl'
      })
      .whenAuthenticated('/makes/:makeId', {
        templateUrl: 'views/makes/make.html',
        controller: 'MakesCtrl'
      })
      .whenAuthenticated('/makes', {
				templateUrl: 'views/makes/makes.html',
				controller: 'MakesCtrl'
      })
      .whenAuthenticated('/promos/add', {
        templateUrl: 'views/promos/promo-add.html',
        controller: 'PromosCtrl'
      })
      .whenAuthenticated('/promos/:makeId', {
        templateUrl: 'views/promos/promo.html',
        controller: 'PromosCtrl'
      })
      .whenAuthenticated('/promos', {
        templateUrl: 'views/promos/promos.html',
        controller: 'PromosCtrl'
      })
      .when('/login', {
				templateUrl: 'views/login.html',
				controller: 'LoginCtrl'
      })
      .otherwise({
        redirectTo: '/quotes'
      });
  })
  .run(['$rootScope', '$location', 'Auth', 'SECURED_ROUTES', 'loginRedirectPath',
    function($rootScope, $location, Auth, SECURED_ROUTES, loginRedirectPath) {
      // watch for login status changes and redirect if appropriate
      Auth.$onAuth(check);
      // some of our routes may reject resolve promises with the special {authRequired: true} error
      // this redirects to the login page whenever that is encountered
      $rootScope.$on('$routeChangeError', function(e, next, prev, err) {
        if( err === 'AUTH_REQUIRED' ) {
          $location.path(loginRedirectPath);
        }
      });

      function check(user) {
        if (!user && authRequired($location.path()) && user.uid === 'google:107383390349213291695') {
          $location.path(loginRedirectPath);
        }
      }

      function authRequired(path) {
        return SECURED_ROUTES.hasOwnProperty(path);
      }
    }
  ])

  // used by route security
  .constant('SECURED_ROUTES', {});
})();