(function() {
'use strict';

angular
  .module('autonlease.newchanet.admin', [
    'ngRoute',
    'ngMaterial',

    'firebase',
    'firebase.config',
    'firebase.ref',
    'firebase.auth',

    'pascalprecht.translate'
  ]);
})();
