(function() {
	'use strict';
	
	angular
		.module('firebase.config', [])
		.constant('FIREBASE_URL', 'https://blazing-torch-5804.firebaseio.com')
		// .constant('FIREBASE_URL', 'https://popping-fire-4462.firebaseio.com')
		.constant('LOGIN_PROVIDERS', ['google'])
		.constant('loginRedirectPath', '/login');
})();