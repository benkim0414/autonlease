(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.config(function($compileProvider, $translateProvider) {
		$compileProvider.debugInfoEnabled(false);
		$translateProvider.translations('kr', {
			'Lease': '리스',
			'Rental': '장기 렌트',

			// makes
			'Chevrolet': '쉐보레',
			'Hyundai': '현대',
			'Kia': '기아',
			'Renault Samsung': '르노 삼성',
			'Ssangyong': '쌍용',

			// models
			'Spark': '스파크',
			'Aveo Sedan': '아베오 세단',
			'Aveo Hatchback': '아베오 해치백',
			'Cruze': '크루즈',
			'Cruze5': '크루즈5',
			'Malibu': '말리부',
			'Impala': '임팔라',
			'Alpheon': '알페온',
			'Trax': '트랙스',
			'Orlando': '올란도',
			'Captiva': '캡티바',
			'Camaro': '카마로',
			'Damas': '다마스',
			'Labo': '라보',
			'Accent': '엑센트',
			'Accent Wit': '엑센트 위트',
			'Avante': '아반떼',
			'Veloster': '벨로스터',
			'Sonata': '쏘나타',
			'Sonata Hybrid': '쏘나타 하이브리드',
			'Sonata Plug-in Hybrid': '쏘나타 플러그인 하이브리드',
			'i40 Saloon': 'i40 살룬',
			'Grandeur': '그랜저',
			'Grandeur Hybrid': '그랜저 하이브리드',
			'Aslan': '아슬란',
			'Genesis': '제네시스',
			'Genesis Coupe': '제네시스 쿠페',
			'Equus': '에쿠스',
			'Tucson': '투싼',
			'Santa Fe': '싼타페',
			'Maxcruz': '맥스크루즈',
			'Morning': '모닝',
			'Pride': '프라이드',
			'K3 Koup': 'K3 쿱',
			'K5 Hybrid 500h': 'K5 하이브리드 500h',
			'K7 Hybrid 700h': 'K7 하이브리드 700h',
			'Ray': '레이',
			'Soul': '쏘울',
			'Soul EV': '쏘울 EV',
			'Carens': '카렌스',
			'Sportage': '스포티지',
			'Sorento': '쏘렌토',
			'Mohave': '모하비',
			'Carnival': '카니발',
			'Carnival Hi-Limousine': '카니발 하이리무진',
			'Carnival Outdoor': '카니발 아웃도어',
			'Tivoli': '티볼리',
			'Chairman W': '체어맨 W',
			'Rexton W': '렉스턴 W',
			'Korando C': '코란도 C',
			'Korando Sports': '코란도 스포츠',
			'Korando Turismo': '코란도 투리스모',
		});

		$translateProvider.preferredLanguage('kr');
	});
})();