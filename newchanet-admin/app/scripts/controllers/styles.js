(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.controller('StylesCtrl', function($scope, $location, $routeParams, $mdToast, $firebaseArray, $firebaseObject, Ref) {
		$scope.makeId = $routeParams.makeId;
		$scope.modelId = $routeParams.modelId;

		var stylesRef = Ref.child('styles').child($routeParams.makeId).child($routeParams.modelId);
		$scope.styles = $firebaseArray(stylesRef);
		$scope.styles.$loaded().then(function() {
			$scope.loaded = true;
		});

		$scope.goStyleEditor = function(styleId) {
			$location.path(['makes', $scope.makeId, 'models', $scope.modelId, 'styles', styleId].join('/'));
		};

		if ($routeParams.styleId !== undefined) {
			var styleRef = Ref.child('styles').child($scope.makeId).child($scope.modelId).child($routeParams.styleId);
			$scope.style = $firebaseObject(styleRef);
			$scope.style.$loaded().then(function() {
				$scope.loaded = true;
			});
		} else {
			var makeRef = Ref.child('makes').child($scope.makeId);
			$scope.make = $firebaseObject(makeRef);

			var modelRef = Ref.child('models').child($scope.makeId).child($scope.modelId);
			$scope.model = $firebaseObject(modelRef);
		}

		function goBack() {
			$location.path(['makes', $scope.makeId, 'models', $scope.modelId, 'styles'].join('/'));
		}

		$scope.cancel = function() {
			goBack();
		};
		$scope.edit = function() {
			$scope.style.$save().then(function() {
				$mdToast.show($mdToast.simple().textContent('The changes have been saved.'));
				goBack();
			});
		};

		$scope.remove = function() {
			var styleName = $scope.style.name;
			$scope.style.$remove().then(function() {
				$mdToast.show($mdToast.simple().textContent(styleName + ' is successfully removed.'));
				goBack();
			});
		};

		$scope.add = function() {
			var stylesRef = Ref.child('styles').child($scope.makeId).child($scope.modelId);
			var styles = $firebaseArray(stylesRef);

			$scope.style.make = $scope.make;
			$scope.style.model = $scope.model;

			styles.$add($scope.style).then(function() {
				$mdToast.show($mdToast.simple().textContent($scope.style.name + ' is successfully added.'));
				goBack();
			});
		};
	});
})();