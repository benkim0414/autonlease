(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.controller('PromosCtrl', function($scope, $location, $routeParams, $timeout, $mdToast, $firebaseArray, $firebaseObject, Ref) {
		var promosRef = Ref.child('promos');
		$scope.promos = $firebaseArray(promosRef);
		$scope.promos.$loaded().then(function() {
			$scope.loaded = true;
		});

		$scope.goPromoEditor = function(makeId) {
			$location.path(['promos', makeId].join('/'));
		};

		if ($routeParams.makeId !== undefined) {
			$scope.makeId = $routeParams.makeId;
			var promoRef = Ref.child('promos').child($scope.makeId);
			$scope.promo = $firebaseObject(promoRef);
			$scope.promo.$loaded().then(function() {
				$scope.loaded = true;
			});
		}

		function goBack() {
			$location.path('promos');
		}
		$scope.cancel = function() {
			goBack();
		};

		$scope.edit = function() {
			$scope.promo.$save().then(function() {
				$mdToast.show($mdToast.simple().textContent('The changes have been saved.'));
				goBack();
			});
		};

		$scope.remove = function() {
			var makeName = $scope.promo.name;
			$scope.promo.$remove().then(function() {
				$mdToast.show($mdToast.simple().textContent(makeName + ' is successfully removed.'));
				goBack();
			});
		};

		$scope.loadMakes = function() {
			return $timeout(function() {
				var makesRef = Ref.child('makes');
				$scope.makes = $firebaseArray(makesRef);
			}, 700);
		};

		$scope.add = function() {
			var id = $scope.promo.make.niceName;
			var promoRef = Ref.child('promos').child(id);
			var promo = $firebaseObject(promoRef);

			$scope.promo.name = $scope.promo.make.name;
			delete $scope.promo.make;
			angular.copy($scope.promo, promo);

			promo.$save().then(function() {
				$mdToast.show($mdToast.simple().textContent($scope.promo.name + ' is successfully added.'));
				goBack();
			});
		};
	});
})();