(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.controller('QuotesCtrl', function($scope, $location, $routeParams, $firebaseArray, $firebaseObject, Ref) {
		if ($routeParams.quoteId !== undefined) {
			var quoteRef = Ref.child('quotes').child($routeParams.quoteId);
			$scope.quote = $firebaseObject(quoteRef);
			$scope.quote.$loaded(function() {
				$scope.loaded = true;
			});
		}

		$scope.done = 0;
		var quotesRef = Ref.child('quotes');
		$scope.quotes = $firebaseArray(quotesRef);
		$scope.quotes.$loaded(function(quotes) {
			angular.forEach(quotes, function(quote) {
				if (!quote.ongoing) {
					$scope.done++;
				}
			});
			$scope.loaded = true;
		});

		$scope.showQuote = function(quoteId) {
			$location.path(['quotes', quoteId].join('/'));
		};
	});
})();