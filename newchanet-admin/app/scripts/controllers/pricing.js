(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.controller('PricingCtrl', function($scope, $location, $routeParams, $mdDialog, $mdToast, $firebaseArray, $firebaseObject, Ref) {
		if ($routeParams.modelId !== undefined) {
			var modelRef = Ref.child('pricing').child($routeParams.makeId).child($routeParams.modelId);
			$scope.model = $firebaseObject(modelRef);
			$scope.model.$loaded().then(function() {
				$scope.loaded = true;
			});
		}

		var pricingRef = Ref.child('pricing').child($routeParams.makeId);
		$scope.models = $firebaseArray(pricingRef);
		$scope.models.$loaded().then(function() {
			$scope.loaded = true;
		});

		$scope.showDialog = function(ev) {
			$mdDialog.show({
				controller: DialogController,
				templateUrl: 'views/pricing/add-dialog.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose: true
			});
		};
		function DialogController($scope, $timeout, $mdDialog, $firebaseArray, Ref) {
			$scope.model = {
			};

			$scope.close = function() {
				$mdDialog.hide();
			};
			$scope.loadMakes = function() {
				return $timeout(function() {
					$scope.makes = $firebaseArray(Ref.child('makes'));
				}, 650);
			};
			$scope.loadModels = function() {
				return $timeout(function() {
					$scope.models = $firebaseArray(Ref.child('models').child($scope.model.make.niceName));
				}, 650);
			};
			$scope.loadStyles = function() {
				return $timeout(function() {
					$scope.styles = $firebaseArray(Ref.child('styles').child($scope.model.make.niceName).child($scope.model.model.niceName));
				}, 650);
			};
			$scope.add = function() {
				console.log($scope.model);
			};
		}

		$scope.cancel = function() {
			$location.path(['pricing', 'makes', $routeParams.makeId, 'models'].join('/'));
		};

		$scope.edit = function() {
			$scope.model.$save().then(function() {
				$mdToast.show(
					$mdToast.simple().textContent('Successfully Edited.')
				);
			});
		};

		$scope.remove = function() {
			$scope.model.$remove().then(function() {
				$mdToast.show(
					$mdToast.simple().textContent('Successfully Removed.')
				);
			});
		};

		$scope.goModel = function(modelId) {
			$location.path(['pricing', 'makes', $routeParams.makeId, 'models', modelId].join('/'));
		};
	});
})();