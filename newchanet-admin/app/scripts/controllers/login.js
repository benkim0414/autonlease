(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.controller('LoginCtrl', function($scope, $location, Auth) {
		$scope.oauthLogin = function(provider) {
			$scope.err = null;
			Auth.$authWithOAuthPopup(provider, {rememberMe: true}).then(redirect, showError);
		};

		function redirect() {
			$location.path('/quotes');
		}
		function showError(err) {
			$scope.err = err;
		}
	});
})();