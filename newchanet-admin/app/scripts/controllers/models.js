(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.controller('ModelsCtrl', function($scope, $location, $routeParams, $mdToast, $firebaseArray, $firebaseObject, Ref) {
		$scope.makeId = $routeParams.makeId;

		var modelsRef = Ref.child('models').child($routeParams.makeId);
		$scope.models = $firebaseArray(modelsRef);
		$scope.models.$loaded().then(function() {
			$scope.loaded = true;
		});

		$scope.goStyles = function(modelId) {
			$location.path(['makes', $scope.makeId, 'models', modelId, 'styles'].join('/'));
		};

		$scope.goModelEditor = function(modelId) {
			$location.path(['makes', $scope.makeId, 'models', modelId].join('/'));
		};

		if ($routeParams.modelId !== undefined) {
			var modelRef = Ref.child('models').child($scope.makeId).child($routeParams.modelId);
			$scope.model = $firebaseObject(modelRef);

			$scope.loaded = true;
		}

		function toNice(name) {
			return angular.lowercase(name.replace(/ /g, '-'));
		}
		function goBack() {
			$location.path(['makes', $scope.makeId, 'models'].join('/'));
		}

		$scope.cancel = function() {
			goBack();
		};
		$scope.edit = function() {
			$scope.model.niceName = toNice($scope.model.name);
			$scope.model.$save().then(function() {
				$mdToast.show($mdToast.simple().textContent('The changes have been saved.'));
				goBack();
			});
		};

		$scope.remove = function() {
			var modelName = $scope.model.name;
			$scope.model.$remove().then(function() {
				$mdToast.show($mdToast.simple().textContent(modelName + ' is successfully removed.'));
				goBack();
			});
		};

		$scope.add = function() {
			$scope.model.niceName = toNice($scope.model.name);
			$scope.model.id = [$scope.makeId, $scope.model.niceName].join('-');

			var modelRef = Ref.child('models').child($scope.makeId).child($scope.model.niceName);
			var model = $firebaseObject(modelRef);

			angular.copy($scope.model, model);

			model.$save().then(function() {
				$mdToast.show($mdToast.simple().textContent($scope.model.name + ' is successfully added.'));
				goBack();
			});
		};
	});
})();