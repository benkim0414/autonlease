(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.controller('LookupCtrl', function($mdSidenav, $location, Auth) {
		this.toggleSidenav = toggleSidenav;
		this.goTo = goTo;
		this.isSelected = isSelected;
		this.auth = Auth.$getAuth();
		// console.log(this.auth);
		this.logout = logout;

		function logout() {
			Auth.$unauth();
			// this.auth = null;
		}
		function toggleSidenav() {
			$mdSidenav('sidenav').toggle();
		}
		function goTo(url) {
			$location.path(url);
			this.toggleSidenav();
		}
		function isSelected(url) {
			if (url === $location.path().slice(0, url.length)) {
				return true;
			} else {
				return false;
			}
		}
	});
})();