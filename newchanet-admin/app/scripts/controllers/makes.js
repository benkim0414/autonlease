(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.controller('MakesCtrl', function($scope, $location, $routeParams, $mdToast, $firebaseArray, $firebaseObject, Ref) {
		var makesRef = Ref.child('makes');
		$scope.makes = $firebaseArray(makesRef);
		$scope.makes.$loaded(function(makes) {
			var DOMESTIC_MAKES = ['hyundai', 'kia', 'chevrolet', 'ssangyong', 'renault-samsung'];
			angular.forEach(makes, function(make) {
				DOMESTIC_MAKES.forEach(function(domesticMake) {
					if (make.niceName === domesticMake) {
						make.isDomestic = true;
					}
				});
			});
			$scope.loaded = true;
		});

		$scope.goModels = function(makeId) {
			$location.path(['makes', makeId, 'models'].join('/'));
		};

		$scope.goMakeEditor = function(makeId) {
			$location.path(['makes', makeId].join('/'));
		};

		if ($routeParams.makeId !== undefined) {
			var makeRef = Ref.child('makes').child($routeParams.makeId);
			$scope.make	= $firebaseObject(makeRef);

			$scope.loaded = true;
		}

		function toNice(name) {
			return angular.lowercase(name.replace(/ /g, '-'));
		}
		function goBack() {
			$location.path('makes');
		}

		$scope.cancel = function() {
			goBack();
		};
		$scope.edit = function() {
			$scope.make.niceName = toNice($scope.make.name);
			$scope.make.$save().then(function() {
				$mdToast.show($mdToast.simple().textContent('The changes have been saved.'));
				goBack();
			});
		};

		$scope.remove = function() {
			var makeName = $scope.make.name;
			$scope.make.$remove().then(function() {
				$mdToast.show($mdToast.simple().textContent(makeName + ' is successfully removed.'));
				goBack();
			});
		};

		$scope.add = function() {
			$scope.make.niceName = toNice($scope.make.name);
			var makeRef = Ref.child('makes').child($scope.make.niceName);
			var make = $firebaseObject(makeRef);
			angular.copy($scope.make, make);
			make.$save().then(function() {
				$mdToast.show($mdToast.simple().textContent($scope.make.name + ' is successfully added.'));
				goBack();
			});
		};
	});
})();