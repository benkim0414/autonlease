(function() {
'use strict';

angular
	.module('autonlease.newchanet.admin')
	.config(function($mdIconProvider) {

		$mdIconProvider
			.icon('action:account_circle', 'images/icons/ic_account_circle_24px.svg')
			.icon('action:announcement', 'images/icons/ic_announcement_24px.svg')
			.icon('action:delete', 'images/icons/ic_delete_24px.svg')
			.icon('action:description', 'images/icons/ic_description_24px.svg')
			.icon('action:done', 'images/icons/ic_done_24px.svg')
			.icon('action:exit_to_app', 'images/icons/ic_exit_to_app_24px.svg')
			.icon('action:info', 'images/icons/ic_info_24px.svg')
			.icon('action:launch', 'images/icons/ic_launch_24px.svg')
			.icon('action:receipt', 'images/icons/ic_receipt_24px.svg')
			.icon('editor:mode_edit', 'images/icons/ic_mode_edit_24px.svg')
			.icon('navigation:close', 'images/icons/ic_close_24px.svg')
			.icon('navigation:menu', 'images/icons/ic_menu_24px.svg')
			.icon('navigation:more_vert', 'images/icons/ic_more_vert_24px.svg')
			.icon('communication:chat', 'images/icons/ic_chat_24px.svg')
			.icon('communication:email', 'images/icons/ic_email_24px.svg')
			.icon('communication:phone', 'images/icons/ic_phone_24px.svg')
			.icon('content:add', 'images/icons/ic_add_24px.svg')
			.icon('content:send', 'images/icons/ic_send_24px.svg')
			.icon('social:person', 'images/icons/ic_person_24px.svg')
			.icon('social:domain', 'images/icons/ic_domain_24px.svg')
			.icon('social:group', 'images/icons/ic_group_24px.svg');
	});
})();