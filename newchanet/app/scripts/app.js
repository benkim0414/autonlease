(function() {
'use strict';

angular
  .module('autonlease.newchanet.app', [
		'ngRoute',
		'ngMaterial',

		'firebase',
		'firebase.config',
		'firebase.ref',
		'firebase.auth',

		'pascalprecht.translate',
		'scrollable-table'
  ]);
})();