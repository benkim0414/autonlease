(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.config(function($routeProvider) {
		$routeProvider
      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'QuotesCtrl'
      })
      .when('/privacy', {
        templateUrl: 'views/privacy.html'
      })
      .when('/counsel/request', {
        templateUrl: 'views/counsel/request.html',
        controller: 'QuotesCtrl'
      })
      .when('/counsel/status', {
        templateUrl: 'views/counsel/status.html',
        controller: 'QuotesCtrl'
      })
      .when('/lease/intro', {
				templateUrl: 'views/lease/intro.html'
      })
      .when('/lease/pricing/makes/:makeId', {
        templateUrl: 'views/lease/pricing/pricing.html',
        controller: 'PricingCtrl'
      })
      .when('/lease/pricing/makes', {
        templateUrl: 'views/lease/pricing/makes.html',
        controller: 'MakesCtrl'
      })
      .when('/rental/intro', {
        templateUrl: 'views/rental/intro.html'
      })
      .when('/rental/pricing/makes/:makeId', {
        templateUrl: 'views/rental/pricing/pricing.html',
        controller: 'PricingCtrl'
      })
      .when('/rental/pricing/makes', {
        templateUrl: 'views/rental/pricing/makes.html',
        controller: 'MakesCtrl'
      })
      .when('/required-docs', {
        templateUrl: 'views/docs.html'
      })
      .when('/promos', {
        templateUrl: 'views/promos.html',
        controller: 'PromosCtrl'
      })
      .when('/quotation/makes/:makeId/models/:modelId/styles/:styleId/request', {
        templateUrl: 'views/quotation/request.html',
        controller: 'QuotesCtrl'
      })
      .when('/quotation/makes/:makeId/models/:modelId/styles', {
        templateUrl: 'views/quotation/styles.html',
        controller: 'StylesCtrl'
      })
      .when('/quotation/makes/:makeId/models', {
        templateUrl: 'views/quotation/models.html',
        controller: 'ModelsCtrl'
      })
      .when('/quotation/makes', {
        templateUrl: 'views/quotation/makes.html',
        controller: 'MakesCtrl'
      })
      .otherwise({
        redirectTo: '/home'
      });
	});
})();