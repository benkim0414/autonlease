(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.directive('scrollShadow', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var content = angular.element(document.querySelector('#' + attrs.scrollShadow));
				content.bind('scroll', function() {
					content[0].scrollTop > 15 ? element.addClass('md-whiteframe-z2') : element.removeClass('md-whiteframe-z2');
				});
			}
		};
	});
})();