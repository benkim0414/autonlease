(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.controller('PromosCtrl', function($window, $scope, $firebaseArray, Ref) {
		var promosRef = Ref.child('promos');
		$scope.promos = $firebaseArray(promosRef);
		$scope.promos.$loaded().then(function(promos) {
			var domesticMakes = ['hyundai', 'kia', 'chevrolet', 'ssangyong', 'renault-samsung'];
			angular.forEach(promos, function(promo) {
				domesticMakes.forEach(function(make) {
					if (promo.$id === make) {
						promo.isDomestic = true;
					}
				});
			});

			$scope.loaded = true;
		});

		$scope.open = function(url) {
			$window.open(url);
		};
	});
})();