(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.controller('RequestCtrl', function($scope, $routeParams, $firebaseObject, Ref) {
		var styleRef = Ref.child('styles').child($routeParams.makeId).child($routeParams.modelId).child($routeParams.styleId);
		$scope.style = $firebaseObject(styleRef);
	});
})();