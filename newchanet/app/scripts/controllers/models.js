(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.controller('ModelsCtrl', function($scope, $location, $routeParams, $firebaseArray, Ref) {
		$scope.makeId = $routeParams.makeId;

		var modelsRef = Ref.child('models').child($routeParams.makeId);
		$scope.models = $firebaseArray(modelsRef);
		$scope.models.$loaded(function() {
			$scope.loaded = true;
		});

		$scope.goStyles = function(modelId) {
			$location.path(['quotation', 'makes', $scope.makeId, 'models', modelId, 'styles'].join('/'));
		};
	});
})();