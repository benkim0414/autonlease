(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.controller('PricingCtrl', function($window, $scope, $routeParams, $firebaseArray, Ref) {
		var pricingRef = Ref.child('pricing').child($routeParams.makeId);
		$scope.models = $firebaseArray(pricingRef);
		$scope.models.$loaded().then(function() {
			$scope.loaded = true;
		});

		$scope.open = function(url) {
			$window.open(url);
		};
	});
})();