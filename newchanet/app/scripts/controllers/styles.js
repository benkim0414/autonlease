(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.controller('StylesCtrl', function($window, $scope, $location, $routeParams, $firebaseArray, Ref) {
		$scope.makeId = $routeParams.makeId;

		var stylesRef = Ref.child('styles').child($routeParams.makeId).child($routeParams.modelId);
		$scope.styles = $firebaseArray(stylesRef);
		$scope.styles.$loaded(function() {
			$scope.loaded = true;
		});

		$scope.goRequest = function(styleId) {
			$location.path(['quotation', 'makes', $scope.makeId, 'models', $routeParams.modelId, 'styles', styleId, 'request'].join('/'));
		};

		$scope.launch = function(make) {
			$window.open($scope.urls[make], '_blank');
		};
		$scope.urls = {
			'chevrolet': 'http://www.chevrolet.co.kr/',
			'hyundai': 'http://www.hyundai.com',
			'kia': 'http://www.kia.com/',
			'renault-samsung': 'http://www.renaultsamsungm.com/',
			'ssangyong': 'http://www.smotor.com/',

			'audi': 'http://www.audi.co.kr/',
			'bmw': 'http://www.bmw.co.kr/',
			'cadillac': 'http://www.cadillac.co.kr/',
			'chrysler': 'http://www.chrysler.co.kr/',
			'fiat': 'http://www.fiat.co.kr/',
			'ford': 'http://www.ford-korea.com/',
			'honda': 'http://www.hondakorea.co.kr/',
			'infiniti': 'http://www.infiniti.co.kr/',
			'jaguar': 'http://www.jaguarkorea.co.kr/',
			'jeep': 'http://www.jeep.co.kr/',
			'land-rover': 'http://www.landroverkorea.co.kr/',
			'lexus': 'http://www.lexus.co.kr/',
			'lincoln': 'http://www.lincoln.com/',
			'maserati': 'http://www.maserati.co.kr/',
			'mercedes-benz': 'http://www.mercedes-benz.co.kr/',
			'mini': 'http://www.mini.co.kr/',
			'nissan': 'http://www.nissan.co.kr/',
			'porsche': 'http://www.porsche.com/korea/ko/',
			'toyota': 'http://www.toyota.co.kr/',
			'volkswagen': 'http://www.volkswagen.co.kr/',
			'volvo': 'http://www.volvocars.com/kr/'
		};
	});
})();