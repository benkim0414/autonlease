(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.controller('QuotesCtrl', function($window, $scope, $location, $routeParams, $mdToast, $firebaseObject, $firebaseArray, Ref) {
		if ($routeParams.makeId) {
			var styleRef = Ref.child('styles').child($routeParams.makeId).child($routeParams.modelId).child($routeParams.styleId);
			$scope.style = $firebaseObject(styleRef);
		}

		$scope.done = 0;
		var quotesRef = Ref.child('quotes');
		$scope.quotes = $firebaseArray(quotesRef);
		$scope.quotes.$loaded(function(quotes) {
			angular.forEach(quotes, function(quote) {
				if (!quote.ongoing) {
					$scope.done++;
				}
			});
			$scope.loaded = true;
		});

		$scope.request = function(quote) {
			var simpleToast = $mdToast.simple().textContent('필수 항목을 입력해 주세요.');
			if (quote === undefined || quote.applicant.name === undefined || quote.applicant.phone === undefined) {
				$mdToast.show(simpleToast);
				return;
			}

			if (quote.applicant.type === undefined) {
				quote.applicant.type = 'Individual';
			}
			quote.ongoing = true;
			quote.created = $window.Firebase.ServerValue.TIMESTAMP;

			var content = '성명: ' + quote.applicant.name + '<br>' +
										'연락처: ' + quote.applicant.phone + '<br>' +
										'신청 내용: ' + quote.notes;
			if ($scope.style !== undefined) {
				var style = $scope.style;
				quote.vehicle = {
					id: style.$id,
					make: style.make,
					model: style.model,
					trim: style.name
				};

				content = content + '<br>' +
									'구분: ' + quote.applicant.type + '<br>' +
									'이메일: ' + quote.applicant.email + '<br><br>' +
									'차량 모델: ' + quote.vehicle.make.name + ' ' + quote.vehicle.model.name + '<br>' +
									'차량 트림: ' + quote.vehicle.trim + '<br>' +
									'이용 상품: ' + quote.financialProduct + '<br>';
			}
			
			$scope.quotes.$add(quote).then(function() {
				var payload = {
					name: 'Hongbok Seo',
					email: 'autonlease@gmail.com',
					message: content
				};
				$window.AWS.config.update({
					accessKeyId: 'AKIAJFCUXTJX53XO5ZLA',
					secretAccessKey: '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'
				});
				$window.AWS.config.region = 'us-east-1';

				var lambda = new $window.AWS.Lambda({apiVersion: '2015-03-31'});
				lambda.invoke({
					FunctionName: 'lambdaSESSendEmail',
					ClientContext: 'ClientContextJSON',
					InvocationType: 'Event',
					LogType: 'Tail',
					Payload: angular.toJson(payload)
				}, function(err, data) {
					if (err) {
						console.log(err, err.stack);
					} else {
						console.log(data);
					}
				});

				$mdToast.show(
					$mdToast.simple().textContent('상담 신청이 완료되었습니다.')
				);
				$location.path('counsel/status');
			});
		};
	});
})();