(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.controller('LookupCtrl', function($location, $mdSidenav) {

		this.toggleSidenav = function() {
			$mdSidenav('sidenav').toggle();
		};

		this.goTo = function(url) {
			$location.path(url);
			this.toggleSidenav();
		};

		this.isSelected = function(url) {
			if (url === $location.path().slice(0, url.length)) {
				return true;
			} else {
				return false;
			}
		};
	});
})();