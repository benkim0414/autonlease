(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.controller('MakesCtrl', function($scope, $location, $firebaseArray, Ref) {
		var makesRef = Ref.child('makes');
		$scope.makes = $firebaseArray(makesRef);
		$scope.makes.$loaded(function(makes) {
			var domestic = ['hyundai', 'kia', 'chevrolet', 'ssangyong', 'renault-samsung'];
			angular.forEach(makes, function(make) {
				domestic.forEach(function(brand) {
					if (make.niceName === brand) {
						make.isDomestic = true;
					}
				});
			});
			$scope.loaded = true;
		});

		$scope.goModels = function(makeId) {
			$location.path(['quotation', 'makes', makeId, 'models'].join('/'));
		};
		$scope.goLeasePricing = function(makeId) {
			$location.path(['lease', 'pricing', 'makes', makeId].join('/'));
		};
		$scope.goRentalPricing = function(makeId) {
			$location.path(['rental', 'pricing', 'makes', makeId].join('/'));
		};
	});
})();