(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.filter('starredName', function() {
		return function(input) {
			var padding = '';
			for (var i = 0; i < input.length - (input.length / 2) - 1; i++) {
				padding = padding.concat('*');
			}
			return input.slice(0, input.length / 2 + 1) + padding;
		};
	});
})();