(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('teal')
      .accentPalette('amber');
  });
})();