(function() {
'use strict';

angular
	.module('autonlease.newchanet.app')
	.config(function($mdIconProvider) {
		$mdIconProvider.fontSet('fa', 'fontawesome');

		$mdIconProvider
			.icon('action:announcement', 'images/icons/ic_announcement_24px.svg')
			.icon('action:copyright', 'images/icons/ic_copyright_24px.svg')
			.icon('action:done', 'images/icons/ic_done_24px.svg')
			.icon('action:info', 'images/icons/ic_info_24px.svg')
			.icon('action:launch', 'images/icons/ic_launch_24px.svg')
			.icon('action:receipt', 'images/icons/ic_receipt_24px.svg')
			.icon('navigation:menu', 'images/icons/ic_menu_24px.svg')
			.icon('navigation:more_vert', 'images/icons/ic_more_vert_24px.svg')
			.icon('communication:chat', 'images/icons/ic_chat_24px.svg')
			.icon('communication:phone', 'images/icons/ic_phone_24px.svg')
			.icon('content:send', 'images/icons/ic_send_24px.svg')
			.icon('hardware:security', 'images/icons/ic_security_24px.svg')
			.icon('social:person', 'images/icons/ic_person_24px.svg')
			.icon('social:domain', 'images/icons/ic_domain_24px.svg')
			.icon('social:group', 'images/icons/ic_group_24px.svg');
	});
})();